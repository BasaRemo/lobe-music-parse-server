// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var cors = require('cors') // add this line below it
var ParseServer = require('parse-server').ParseServer;
var path = require('path');


var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://admin:password@ds141519-a0.mlab.com:41519,ds141519-a1.mlab.com:41519/lobe-music-mlab-db?replicaSet=rs-ds141519',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'DlIbcE0Xiy5upPCDpZXneHfAD76lDBiGvwcT51ee',
  masterKey: process.env.MASTER_KEY || 'Xlbt9tGnTBhgywUn6h8RsqmrkckndKCqnLadyI7l', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'https://lobemusic.herokuapp.com/parse',  // Don't forget to change to https if needed
  facebookAppIds : [1283244075057524],
  oauth: { 
    facebook: { 
      appIds: [1283244075057524]
    },
    twitter: {
          consumer_key: "RNjmmzejGFfXOpZ5lgXH6l3Mc",
          consumer_secret: "tfzxQB8fqUO9UbCRJwodB4G0X8fIUbEf6mfptqFGwoHRg6DpE0"
    },
    spotify: {
          consumer_key: "52aeb90fd50f4cb9a4f7922dbba3b089",
          consumer_secret: "52aeb90fd50f4cb9a4f7922dbba3b089"
    }
  },
  push: {
    android: {
      senderId: '123', // The Sender ID of GCM
      apiKey: '123' // The Server API Key of GCM
    },
    ios: {
      pfx: 'cloud/certificates/Certificates.p12', // The filename of private key and certificate in PFX or PKCS12 format from disk  
      bundleId: 'appymood.lobe', // The bundle identifier associate with your app
      production: false // Specifies which environment to connect to: Production (if true) or Sandbox
    }
  },
    // Enable email verification
  verifyUserEmails: true,
  // The public URL of your app.
  // This will appear in the link that is used to verify email addresses and reset passwords.
  // Set the mount path as it is in serverURL
  publicServerURL: 'https://lobemusic.herokuapp.com/parse',
  // Your apps name. This will appear in the subject and body of the emails that are sent.
  appName: 'Lobe Music',
  // The email adapter
  emailAdapter: {
    module: 'parse-server-simple-mailgun-adapter',
    options: {
      // The address that your emails come from
      fromAddress: 'lobe.appymood@gmail.com',
      // Your domain from mailgun.com
      domain: 'lobemusic.com',
      // Your API key from mailgun.com
      apiKey: 'key-a2e522ce0849e9e86944c7b4522d597d',
    }
  }
});


// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();
app.use(cors()); 
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });
// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
